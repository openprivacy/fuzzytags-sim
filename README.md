# fuzzytags-sim

A playground simulator for [fuzzytags](https://crates.io/crates/fuzzytags) designed to demonstrate the impact of
integrating fuzzytags into an application with poor parameter choices (and aid in choosing good parameters).

## Setup

Run `cargo build --release`, then the binary will be at `./target/release/fuzzytags-sim`

By default, the simulator will run a single-round experiment with 10 parties (`--num-parties`). Messages for parties will
be drawn randomly from a pareto distribution (scale: 1.0, shape: 1.0) i.e. a few parties will receive
many messages, the rest will receive a small number (or zero). This can be adjusted by the `--samples-per-round`
argument.

Once all the parties and messages have been generated, the server will test all messages against all parties.

Finally, the server will produce statistics for the round, including the ideal false positive rate for each party,
the observed match rate for each party, the skew between the ideal false positive rate and the observed rate, and the
number of trivial attributions it has made to that party in this round.

Note: A *trivial attribution* corresponds to the server receiving a tag which only matches a single party.

## Future Extensions

- Multiple Rounds / Aggregate Statistics
- Differential Attacks e.g. leak that a set of messages all belong to the same party and have the server guess which party
they belong to.
- Simulate poisson distribution arrival (and bidirectional conversation) and allow the server to make timing correlations to
aid in breaking anonymity