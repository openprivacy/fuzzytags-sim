/// Welcome to the probability theory module!
/// In this module we encapsulate a bunch of different theory useful for probabilistic modelling

/// Calculating properties of binomial distributions
/// e.g. the probability of flipping heads a least 48 times out of 100 flips.
pub mod binomial;

/// Calculating properties of hypergeometric distributions
/// e.g. the chance of drawing at least 2 red balls from a bucket containing 4 red balls and 6 non
/// red balls.
pub mod hypergeometric;
